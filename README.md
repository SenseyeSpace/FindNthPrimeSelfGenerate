# Find nth prime by auto generated code

##### Conclusion:
This is attempt to optimize find Nth big prime number from [FirstTenThousandPrimeNumbers](https://gitlab.com/SenseyeSpace/FirstTenThousandPrimeNumbers), but this attempt is slower than source,
and after try [Disable array/slice bounds checking in Golang to improve performance](https://stackoverflow.com/questions/49972319/disable-array-slice-bounds-checking-in-golang-to-improve-performance) slower too.