package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("need input N")
	}

	sourceN := os.Args[1]

	n, err := strconv.ParseUint(sourceN, 10, 32)

	if err != nil {
		log.Fatalf("invalid parameter n %+v", err)
	}

	relative := "./src/find_nth_prime.go"

	filePath, err := filepath.Abs(relative)

	if err != nil {
		log.Fatalf("absolute path: %+v", err)
	}

	file, err := os.Create(filePath)

	if err != nil {
		log.Fatalf("on open file: %+v", err)
	}

	_, err = file.Write([]byte(fmt.Sprintf(`
package main

import (
	"fmt"
	"math"
)

const max = math.MaxUint32 / 2
const n = %d

func main() {
	fmt.Print(FindNthPrime())
}

func FindNthPrime() uint {
	switch n {
	case 0, 1, 2, 3:
		return n
	}

	return search()
}

func search() uint {
	processing := [n]uint{2, 3}

	length := 2
	last := processing[length-1]

main:
	for next := last + 2; next < max; next += 2 {
		sqrt := uint(math.Sqrt(float64(next)))

		for i := 0; i < length; i++ {
			prime := processing[i]

			if prime > sqrt {
				processing[length] = next

				length++

				if length == n {
					return processing[length-1]
				}

				continue main
			}

			if next%%prime == 0 {
				continue main
			}
		}

		processing[length] = next

		length++

		if length == n {
			return processing[length-1]
		}
	}

	return 0
}
`, n)))

	if err != nil {
		log.Fatalf("on write file: %+v", err)
	}

	file.Close()

	cmd := exec.Command("go", "run", relative)
	out := new(bytes.Buffer)
	cmd.Stdout = out

	start := time.Now()
	err = cmd.Run()
	fmt.Printf("FindNtnPrime: %s\n", out.String())

	if err != nil {
		log.Fatalf("on run file: %+v", err)
	}

	duration := time.Now().Sub(start).Nanoseconds()
	fmt.Printf("Duration: %d ns\n", duration)
}
