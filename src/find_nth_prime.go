package main

import (
	"fmt"
	"math"
)

const max = math.MaxUint32 / 2
const n = 100001

func main() {
	fmt.Print(FindNthPrime())
}

func FindNthPrime() uint {
	switch n {
	case 0, 1, 2, 3:
		return n
	}

	return search()
}

func search() uint {
	var processing = [n]uint{2, 3}

	length := 2
	last := processing[length-1]

next:
	for next := last + 2; next < max; next += 2 {
		sqrt := uint(math.Sqrt(float64(next)))

		for i := 0; i < length; i++ {
			prime := processing[i]

			if prime > sqrt {
				break
			}

			if next%prime == 0 {
				continue next
			}
		}

		processing[length] = next

		length++

		if length == n {
			return processing[length-1]
		}
	}

	return 0
}
