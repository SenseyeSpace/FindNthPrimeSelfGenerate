package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindNthPrime(t *testing.T) {
	if n != 100001 {
		t.Fail()
	}

	assert.Equal(t, uint(1299721), FindNthPrime())
}

func BenchmarkFindNthPrime(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FindNthPrime()
	}
}
